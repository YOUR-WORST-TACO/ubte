#!/usr/bin/env bash

############################################################
# ▞▀▖               ▐      ▞▀▖      ▗▀▖▗    
# ▌▄▖▞▀▖▛▀▖▞▀▖▙▀▖▝▀▖▜▀ ▞▀▖ ▌  ▞▀▖▛▀▖▐  ▄ ▞▀▌
# ▌ ▌▛▀ ▌ ▌▛▀ ▌  ▞▀▌▐ ▖▛▀  ▌ ▖▌ ▌▌ ▌▜▀ ▐ ▚▄▌
# ▝▀ ▝▀▘▘ ▘▝▀▘▘  ▝▀▘ ▀ ▝▀▘ ▝▀ ▝▀ ▘ ▘▐  ▀▘▗▄▘
############################################################
############################################################

# Config Dir
config_dir="${XDG_CONFIG_HOME:-$HOME/.config}/ubte"

if [[ ! -f "$config_dir"/ubte.cfg ]]; then
    mkdir -p "$config_dir"
    cat <<EOF > "$config_dir"/ubte.cfg
# MY VERY NICE CONFIG
# empty for now
EOF
fi

source "$config_dir"/ubte.cfg

############################################################
# ▞▀▖               ▜ 
# ▌▄▖▞▀▖▛▀▖▞▀▖▙▀▖▝▀▖▐ 
# ▌ ▌▛▀ ▌ ▌▛▀ ▌  ▞▀▌▐ 
# ▝▀ ▝▀▘▘ ▘▝▀▘▘  ▝▀▘ ▘
############################################################
############################################################

DEBUG=true
DEBUG_COUNT=0

BACKEND_LOOP=""

CUR_X=0
CUR_Y=0

if [ "$DEBUG" == true ] && [[ -f "$config_dir"/ubte-log.txt ]]; then
	echo "" > "$config_dir"/ubte-log.txt
fi

dblog()
{
    if [ "$DEBUG" == true ]; then
        echo "DEBUG LOG <$DEBUG_COUNT>: $@" >> "$config_dir"/ubte-log.txt
		DEBUG_COUNT=$(( DEBUG_COUNT + 1 ))
    fi
}

closeUbte()
{
    echo "Terminating ubte"
	kill $BACKEND_LOOP
    exit 0
}

############################################################
# ▞▀▖          ▛▀▘▗▜    
# ▌ ▌▛▀▖▞▀▖▛▀▖ ▙▄ ▄▐ ▞▀▖
# ▌ ▌▙▄▘▛▀ ▌ ▌ ▌  ▐▐ ▛▀ 
# ▝▀ ▌  ▝▀▘▘ ▘ ▘  ▀▘▘▝▀▘
############################################################
############################################################

openUbteFile()
{
    echo "IN dev"
}

############################################################
# ▌ ▌            
# ▌ ▌▞▀▘▝▀▖▞▀▌▞▀▖
# ▌ ▌▝▀▖▞▀▌▚▄▌▛▀ 
# ▝▀ ▀▀ ▝▀▘▗▄▘▝▀▘
############################################################
############################################################

usage()
{
    printf "%s\n\t%s\n\t%s\n\t\t%s\n" \
        "ubte - Unnecessary Bash Text Editor" \
        "A console based text editor written in bash" \
        "Usage: ubte [OPTIONS] <FILE> [OPTIONS]" \
        "-h          --help                    display this message"
}

############################################################
# ▞▀▖    ▗▐     ▌        
# ▚▄ ▌  ▌▄▜▀ ▞▀▖▛▀▖▞▀▖▞▀▘
# ▖ ▌▐▐▐ ▐▐ ▖▌ ▖▌ ▌▛▀ ▝▀▖
# ▝▀  ▘▘ ▀▘▀ ▝▀ ▘ ▘▝▀▘▀▀ 
############################################################
############################################################

while [[ ! "$#" -eq 0 ]]; do
    case "$1" in
        "-h"|"--help" )
            usage;
            exit
            ;;
        * )
            if [[ ${1:0:1} = "-" ]]; then
                echo "Error, unrecognized command $1, try fetch -h"
                exit 1
            fi
            if [[ -f $1 ]]; then
                openUbteFile
            fi
            ;;
    esac
    shift
done

#################################################################
# ┏┳┓┏━┓╻ ╻┏━╸┏┳┓┏━╸┏┓╻╺┳╸   ┏━╸┏━┓┏┳┓┏┳┓┏━┓┏┓╻╺┳┓┏━┓
# ┃┃┃┃ ┃┃┏┛┣╸ ┃┃┃┣╸ ┃┗┫ ┃    ┃  ┃ ┃┃┃┃┃┃┃┣━┫┃┗┫ ┃┃┗━┓
# ╹ ╹┗━┛┗┛ ┗━╸╹ ╹┗━╸╹ ╹ ╹    ┗━╸┗━┛╹ ╹╹ ╹╹ ╹╹ ╹╺┻┛┗━┛
#################################################################
#################################################################
# followig commands
#       - up                -- move cursor up
#       - down              -- move cursor down
#       - left              -- move cursor left
#       - right             -- move cursor right
#       - move              -- move to (line, column)
#       - resetcursor       -- move cursor to (0, 0)
#       - savecursor        -- save cursor position
#       - restorecursor     -- move cursor back
#################################################################
_u="\033[1A"
_d="\033[1B"
_l="\033[1D"
_r="\033[1C"

up(){ echo -en "\033[$1A"; }
down(){ echo -en "\033[$1B";}
left(){ echo -en "\033[$1D"; }
right(){ echo -en "\033[$1C"; }
move(){ echo -en "\033[$1;$2H"; }
resetcursor(){ echo -en "\033[2J"; }
savecursor(){ echo -en "\033[s"; }
restorecursor(){ echo -en "\033[u"; }

# NEW STUFF
flush(){ echo -en "\033[2J"; }
width(){ tput cols; }
height(){ tput lines; }

############################################################
# ▌ ▌       ▌ ▌        ▌▜          
# ▙▞ ▞▀▖▌ ▌ ▙▄▌▝▀▖▛▀▖▞▀▌▐ ▞▀▖▙▀▖▞▀▘
# ▌▝▖▛▀ ▚▄▌ ▌ ▌▞▀▌▌ ▌▌ ▌▐ ▛▀ ▌  ▝▀▖
# ▘ ▘▝▀▘▗▄▘ ▘ ▘▝▀▘▘ ▘▝▀▘ ▘▝▀▘▘  ▀▀ 
############################################################
############################################################

onBackspace()
{
	echo "BACKSPACE!!!!"
}

onKeyPress()
{
	echo "PRessed key $@"
}

############################################################
# ▌ ▌                      ▙▗▌                         ▐  
# ▙▞ ▞▀▖▌ ▌▛▀▖▙▀▖▞▀▖▞▀▘▞▀▘ ▌▘▌▝▀▖▛▀▖▝▀▖▞▀▌▞▀▖▛▚▀▖▞▀▖▛▀▖▜▀ 
# ▌▝▖▛▀ ▚▄▌▙▄▘▌  ▛▀ ▝▀▖▝▀▖ ▌ ▌▞▀▌▌ ▌▞▀▌▚▄▌▛▀ ▌▐ ▌▛▀ ▌ ▌▐ ▖
# ▘ ▘▝▀▘▗▄▘▌  ▘  ▝▀▘▀▀ ▀▀  ▘ ▘▝▀▘▘ ▘▝▀▘▗▄▘▝▀▘▘▝ ▘▝▀▘▘ ▘ ▀ 
############################################################
############################################################

IFS=''
ESCAPED_SEQ=""
re='^[0-9]+$'

stty -ixon

escHandle()
{
	read -r -s -n 1 nextkey
	if [[ $nextkey == "[" ]] || [[ $nextkey == "O" ]]; then
		ESCAPED_SEQ="E"
		read -r -s -n 1 nextkey
		if [[ $nextkey =~ $re ]]; then
			ESCAPED_SEQ+="$nextkey"
			while true; do
				read -r -s -n 1 nextkey
				if [[ $nextkey =~ $re ]]; then 
					ESCAPED_SEQ+="$nextkey"
				elif [[ $nextkey == "~" ]]; then
					break
				else
					ESCAPED_SEQ=""
					break
				fi
			done
		else
			ESCAPED_SEQ+="$nextkey"
		fi
	else
		ESCAPED_SEQ=$nextkey
	fi
}

readInput()
{
    read -r -s -n 1 key

	ESCAPED_SEQ=""

	if [[ $key == $'\033' ]]; then
		escHandle
		key="$ESCAPED_SEQ"
	fi

	#LC_CTYPE=C printf '%d ::: ' "'$key'" # determine the key code

	case $key in
		$'\177' ) 	# BACKSPACE KEY
			dblog "BACKSPACE"
			onBackspace
			;;
			
		$'\t' )   	# TAB KEY
			dblog "TAB"
			;;

        $'\021' )   # CTRL + Q COMBO
            closeUbte
            ;;

		$'\023' ) 	# CTRL + S COMBO
			dblog "CTRL+S"
			;;

		$'\0' )		# ENTER KEY
			dblog "ENTER"
			;;

		"E2" )		# INSERT KEY
			dblog "INSERT"
			;;

		"E3" ) 		# DELETE KEY
			dblog "DELETE"
			;;

		"E5" )		# PAGE UP KEY
			dblog "PAGE UP"
			;;

		"E6" )		# PAGE DOWN KEY
			dblog "PAGE DOWN"
			;;

		"EA" )		# ARROW UP KEY
			dblog "ARROW UP"
			;;

		"EB" )		# ARROW DOWN KEY
			dblog "ARROW DOWN"
			;;

		"EC" )		# ARROW RIGHT KEY
			dblog "ARROW RIGHT"
			;;

		"ED" )		# ARROW LEFT KEY
			dblog "ARROW LEFT"
			;;

		"EH" ) 		# HOME KEY
			dblog "HOME"
			;;

		"EF" )		# END KEY
			dblog "END"
			;;

		"EP" ) 		# F1 KEY
			dblog "F1"
			;;

		"EQ" )		# F2 KEY
			dblog "F2"
			;;

		"ER" )		# F3 KEY
			dblog "F3"
			;;

		"ES" )		# F4 KEY
			dblog "F4"
			;;

		"E15" )		# F5 KEY
			dblog "F5"
			;;

		"E17" )		# F6 KEY
			dblog "F6"
			;;

		"E18" )		# F7 KEY
			dblog "F7"
			;;

		"E19" )		# F8 KEY
			dblog "F8"
			;;

		"E20" )		# F9 KEY
			dblog "F9"
			;;

		"E21" )		# F10 KEY
			dblog "F10"
			;;

		"E23" )		# F11 KEY
			dblog "F11"
			;;

		"E24" )		# F12 KEY
			dblog "F12"
			;;

		* )			# NORMAL KEY
			dblog "$key"
			onKeyPress $key
			echo "$MYVAL"
			;;
	esac
}

############################################################
# ▛▀▖          ▗        ▙▗▌   ▐  ▌       ▌   
# ▌ ▌▙▀▖▝▀▖▌  ▌▄ ▛▀▖▞▀▌ ▌▘▌▞▀▖▜▀ ▛▀▖▞▀▖▞▀▌▞▀▘
# ▌ ▌▌  ▞▀▌▐▐▐ ▐ ▌ ▌▚▄▌ ▌ ▌▛▀ ▐ ▖▌ ▌▌ ▌▌ ▌▝▀▖
# ▀▀ ▘  ▝▀▘ ▘▘ ▀▘▘ ▘▗▄▘ ▘ ▘▝▀▘ ▀ ▘ ▘▝▀ ▝▀▘▀▀ 
############################################################
############################################################

drawStatusBar()
{
	echo "Test"
}

############################################################
# ▞▀▖   ▐     ▌   ▛▀▘   ▗▐  
# ▌  ▝▀▖▜▀ ▞▀▖▛▀▖ ▙▄ ▚▗▘▄▜▀ 
# ▌ ▖▞▀▌▐ ▖▌ ▖▌ ▌ ▌  ▗▚ ▐▐ ▖
# ▝▀ ▝▀▘ ▀ ▝▀ ▘ ▘ ▀▀▘▘ ▘▀▘▀ 
############################################################
############################################################

stopExit()
{
    code=$?
    dblog "No! dont exit!"
}

trap stopExit SIGINT

############################################################
# ▛▀▖       ▌           
# ▙▄▘▌ ▌▛▀▖ ▌  ▞▀▖▞▀▖▛▀▖
# ▌▚ ▌ ▌▌ ▌ ▌  ▌ ▌▌ ▌▙▄▘
# ▘ ▘▝▀▘▘ ▘ ▀▀▘▝▀ ▝▀ ▌  
############################################################
############################################################

MYVAL=0
while true; do
	MYVAL=$(( MYVAL + 1 ))
	echo "$MYVAL"
	sleep 0.5
done &
BACKEND_LOOP="$!"

flush
move 0 0
while true; do
    readInput
done