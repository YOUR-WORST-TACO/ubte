#!/usr/bin/env bash

IFS=''
ESCAPED_SEQ=""
re='^[0-9]+$'

stty -ixon

escHandle()
{
	read -r -s -n 1 nextkey
	if [[ $nextkey == "[" ]] || [[ $nextkey == "O" ]]; then
		ESCAPED_SEQ="E"
		read -r -s -n 1 nextkey
		if [[ $nextkey =~ $re ]]; then
			ESCAPED_SEQ+="$nextkey"
			while true; do
				read -r -s -n 1 nextkey
				if [[ $nextkey =~ $re ]]; then 
					ESCAPED_SEQ+="$nextkey"
				elif [[ $nextkey == "~" ]]; then
					break
				else
					ESCAPED_SEQ=""
					break
				fi
			done
		else
			ESCAPED_SEQ+="$nextkey"
		fi
	else
		ESCAPED_SEQ=$nextkey
	fi
}

dblog() {
    X=1
}

onKeyPress()
{
    echo "$@" >> ".\magicBuffer"
}

readInput()
{
    read -r -s -n 1 key

	ESCAPED_SEQ=""

	if [[ $key == $'\033' ]]; then
		escHandle
		key="$ESCAPED_SEQ"
	fi

	#LC_CTYPE=C printf '%d ::: ' "'$key'" # determine the key code

	case $key in
		$'\177' ) 	# BACKSPACE KEY
			dblog "BACKSPACE"
			onBackspace
			;;
			
		$'\t' )   	# TAB KEY
			dblog "TAB"
			;;

        $'\021' )   # CTRL + Q COMBO
            closeUbte
            ;;

		$'\023' ) 	# CTRL + S COMBO
			dblog "CTRL+S"
			;;

		$'\0' )		# ENTER KEY
			dblog "ENTER"
			;;

		"E2" )		# INSERT KEY
			dblog "INSERT"
			;;

		"E3" ) 		# DELETE KEY
			dblog "DELETE"
			;;

		"E5" )		# PAGE UP KEY
			dblog "PAGE UP"
			;;

		"E6" )		# PAGE DOWN KEY
			dblog "PAGE DOWN"
			;;

		"EA" )		# ARROW UP KEY
			dblog "ARROW UP"
			;;

		"EB" )		# ARROW DOWN KEY
			dblog "ARROW DOWN"
			;;

		"EC" )		# ARROW RIGHT KEY
			dblog "ARROW RIGHT"
			;;

		"ED" )		# ARROW LEFT KEY
			dblog "ARROW LEFT"
			;;

		"EH" ) 		# HOME KEY
			dblog "HOME"
			;;

		"EF" )		# END KEY
			dblog "END"
			;;

		"EP" ) 		# F1 KEY
			dblog "F1"
			;;

		"EQ" )		# F2 KEY
			dblog "F2"
			;;

		"ER" )		# F3 KEY
			dblog "F3"
			;;

		"ES" )		# F4 KEY
			dblog "F4"
			;;

		"E15" )		# F5 KEY
			dblog "F5"
			;;

		"E17" )		# F6 KEY
			dblog "F6"
			;;

		"E18" )		# F7 KEY
			dblog "F7"
			;;

		"E19" )		# F8 KEY
			dblog "F8"
			;;

		"E20" )		# F9 KEY
			dblog "F9"
			;;

		"E21" )		# F10 KEY
			dblog "F10"
			;;

		"E23" )		# F11 KEY
			dblog "F11"
			;;

		"E24" )		# F12 KEY
			dblog "F12"
			;;

		* )			# NORMAL KEY
			dblog "$key"
			onKeyPress $key
			echo "$MYVAL"
			;;
	esac
}

while true; do
    readInput
done
CORE_PROC="$!"

cleanup()
{
    kill $CORE_PROC
}
trap cleanup EXIT

while true; do
    read -r -s -n 1 nextkey
    echo "BLEH"
    sleep 1
done