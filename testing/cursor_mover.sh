#!/usr/bin/env bash

# Borrowed from my fetch script
#################################################################
# ┏┳┓┏━┓╻ ╻┏━╸┏┳┓┏━╸┏┓╻╺┳╸   ┏━╸┏━┓┏┳┓┏┳┓┏━┓┏┓╻╺┳┓┏━┓
# ┃┃┃┃ ┃┃┏┛┣╸ ┃┃┃┣╸ ┃┗┫ ┃    ┃  ┃ ┃┃┃┃┃┃┃┣━┫┃┗┫ ┃┃┗━┓
# ╹ ╹┗━┛┗┛ ┗━╸╹ ╹┗━╸╹ ╹ ╹    ┗━╸┗━┛╹ ╹╹ ╹╹ ╹╹ ╹╺┻┛┗━┛
#################################################################
#################################################################
# followig commands
#       - up                -- move cursor up
#       - down              -- move cursor down
#       - left              -- move cursor left
#       - right             -- move cursor right
#       - move              -- move to (line, column)
#       - resetcursor       -- move cursor to (0, 0)
#       - savecursor        -- save cursor position
#       - restorecursor     -- move cursor back
#################################################################
_u="\033[1A"
_d="\033[1B"
_l="\033[1D"
_r="\033[1C"

up(){ echo -en "\033[$1A"; }
down(){ echo -en "\033[$1B";}
left(){ echo -en "\033[$1D"; }
right(){ echo -en "\033[$1C"; }
move(){ echo -en "\033[$1;$2H"; }
resetcursor(){ echo -en "\033[2J"; }
savecursor(){ echo -en "\033[s"; }
restorecursor(){ echo -en "\033[u"; }

# NEW STUFF
flush(){ echo -en "\033[2J"; }
width(){ tput cols; }
height(){ tput lines; }
