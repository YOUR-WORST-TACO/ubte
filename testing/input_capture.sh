#!/usr/bin/env bash

IFS=''
ESCAPED_SEQ=""
re='^[0-9]+$'

stty -ixon

escHandle() {
	read -r -s -n 1 nextkey
	if [[ $nextkey == "[" ]] || [[ $nextkey == "O" ]]; then
		ESCAPED_SEQ="E"
		read -r -s -n 1 nextkey
		if [[ $nextkey =~ $re ]]; then
			ESCAPED_SEQ+="$nextkey"
			while true; do
				read -r -s -n 1 nextkey
				if [[ $nextkey =~ $re ]]; then 
					ESCAPED_SEQ+="$nextkey"
				elif [[ $nextkey == "~" ]]; then
					break
				else
					ESCAPED_SEQ=""
					break
				fi
			done
		else
			ESCAPED_SEQ+="$nextkey"
		fi
	else
		ESCAPED_SEQ=$nextkey
	fi
}

while true; do
	read -r -s -n 1 key

	ESCAPED_SEQ=""

	if [[ $key == $'\033' ]]; then
		escHandle
		key="$ESCAPED_SEQ"
	fi

	LC_CTYPE=C printf '%d ::: ' "'$key'" # determine the key code

	case $key in
		$'\177') 	# BACKSPACE KEY
			echo "BACKSPACE"
			;;
			
		$'\t')   	# TAB KEY
			echo "TAB"
			;;

		$'\023') 	# CTRL + S COMBO
			echo "CTRL+S"
			;;

		$'\0')		# ENTER KEY
			echo "ENTER"
			;;

		"E2")		# INSERT KEY
			echo "INSERT"
			;;

		"E3") 		# DELETE KEY
			echo "DELETE"
			;;

		"E5")		# PAGE UP KEY
			echo "PAGE UP"
			;;

		"E6")		# PAGE DOWN KEY
			echo "PAGE DOWN"
			;;

		"EA")		# ARROW UP KEY
			echo "ARROW UP"
			;;

		"EB")		# ARROW DOWN KEY
			echo "ARROW DOWN"
			;;

		"EC")		# ARROW RIGHT KEY
			echo "ARROW RIGHT"
			;;

		"ED")		# ARROW LEFT KEY
			echo "ARROW LEFT"
			;;

		"EH") 		# HOME KEY
			echo "HOME"
			;;

		"EF")		# END KEY
			echo "END"
			;;

		"EP") 		# F1 KEY
			echo "F1"
			;;

		"EQ")		# F2 KEY
			echo "F2"
			;;

		"ER")		# F3 KEY
			echo "F3"
			;;

		"ES")		# F4 KEY
			echo "F4"
			;;

		"E15")		# F5 KEY
			echo "F5"
			;;

		"E17")		# F6 KEY
			echo "F6"
			;;

		"E18")		# F7 KEY
			echo "F7"
			;;

		"E19")		# F8 KEY
			echo "F8"
			;;

		"E20")		# F9 KEY
			echo "F9"
			;;

		"E21")		# F10 KEY
			echo "F10"
			;;

		"E23")		# F11 KEY
			echo "F11"
			;;

		"E24")		# F12 KEY
			echo "F12"
			;;

		* )			# NORMAL KEY
			echo "$key"
			;;
	esac
done